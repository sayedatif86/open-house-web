import React from 'react';
import axios from 'axios';
import './App.css';

class App extends React.Component {
  state = {
    input: '',
    error: false
  }

  componentDidMount = async () => {
    if (window.location && window.location.href) {
      const urlArr = window.location.href.split('/');
      const slug = urlArr[urlArr.length - 1]
      const obj = {
        method: 'get',
        url: `http://localhost:8080/${slug}`,
        headers: {
          'Access-Control-Allow-Origin': '*',
        }
      }
      const { data } = await axios(obj);
      if (data && data.redirectUrl) {
        if (data.redirectUrl.indexOf('http') > -1) {
          window.location = data.redirectUrl;
        } else {
          window.location = `https://${data.redirectUrl}`;
        }
      }
    }
  }

  handleChange = e => {
    const val = e.target.value;
    this.setState({
      input: val
    })
  }

  handleClick = async () => {
    const { input } = this.state
    if (!input || input.length === 0) {
      this.setState({ error: true })
      return;
    }
    try {
      const obj = {
        method: 'post',
        url: 'http://localhost:8080/save',
        data: {
          url: input
        },
        headers: {
          'Access-Control-Allow-Origin': '*',
        }
      }
      const data = await axios(obj);
    } catch(e) {
      console.log('Error', e);
    }
  }

  render() {
    const { input } = this.state;
    return (
      <div className="container">
        <input className="input-box" onChange={this.handleChange} value={input} />
        <button className="btn" onClick={this.handleClick}>Save</button>
      </div>
    )
  }
}

export default App;
